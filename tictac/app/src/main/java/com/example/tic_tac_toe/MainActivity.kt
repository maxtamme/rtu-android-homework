package com.example.tic_tac_toe

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageButton

class MainActivity : AppCompatActivity() {

    lateinit var button : Button;
    lateinit var comp : Button;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button = findViewById(R.id.button);
        button.setOnClickListener {
            val intent = Intent(MainActivity@this, MainActivity4::class.java)
            startActivity(intent)
        }

        comp = findViewById(R.id.buttonAI);
        comp.setOnClickListener {
            val intent2 = Intent(MainActivity@this, MainActivity5::class.java)
            startActivity(intent2)
        }

    }
}