package com.example.tic_tac_toe

import android.R.attr
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity


class MainActivity2 : AppCompatActivity() {

    lateinit var buttons : Array<Array<ImageButton>>;
    lateinit var plScore1 : TextView;
    lateinit var plScore2 : TextView;

    //0 = empty; 1 = pl; 2 =pl2;
    lateinit var filled : Array<Array<Int>>

    private var pl1Turn : Boolean = true;
    private var score1 : Int = 0;
    private var score2 : Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_content)

        buttons[0][0] = findViewById(R.id.imageButton1);
        buttons[0][1] = findViewById(R.id.imageButton2);
        buttons[0][2] = findViewById(R.id.imageButton3);
        buttons[1][0] = findViewById(R.id.imageButton4);
        buttons[1][1] = findViewById(R.id.imageButton5);
        buttons[1][2] = findViewById(R.id.imageButton6);
        buttons[2][0] = findViewById(R.id.imageButton7);
        buttons[2][1] = findViewById(R.id.imageButton8);
        buttons[2][2] = findViewById(R.id.imageButton9);

        //plScore1 = findViewById(android.R.id.);
        //plScore2 = findViewById(android.R.id.score2);

        var j : Int = 0;

        for ((i, arr) in buttons.withIndex()) {
            j = 0;
            for (button in arr) {
                buttons[i][j].setOnClickListener { action(i , j); }
                j++;
            }
        }

        for ((i, arr) in filled.withIndex()) {
            j = 0;
            for (zxc in arr) {
                filled[i][j] = 0;
                j++;
            }
        }

    }

    private fun action(i : Int, j : Int) {
        fill(i, j);
        check()
    }

    private fun fill(i : Int, j : Int) {
        if (filled[i][j] != 0) {
            return;
        }
        if (pl1Turn) {
            filled[i][j] = 1;
            pl1Turn = false;
        } else {
            filled[i][j] = 2;
            pl1Turn = true;
        }
        changeColor(i, j);
    }

    private fun changeColor(i: Int, j: Int) {
        buttons[i][j].setBackgroundResource(R.drawable.ic_launcher_background)
    }

    private fun check() {

        var j : Int = 0;
        var i : Int = 0;

        for ((i, arr) in filled.withIndex()) {
            j = 0;
                if (filled[i][j] == 1 && filled[i][j+1] == 1 && filled[i][j+2] == 1) {
                    this.score1++;
                    //p1 win
                }
                if (filled[i][j] == 2 && filled[i][j+1] == 2 && filled[i][j+2] == 2) {
                    this.score2++;
                    //p2 win
                }

        }

        for ((j, arr) in filled.withIndex()) {
            i = 0;
            if (filled[j][i] == 1 && filled[j][i+1] == 1 && filled[j][i+2] == 1) {
                this.score1++;
                //p1 win
            }
            if (filled[j][i] == 2 && filled[j][i+1] == 2 && filled[j][i+2] == 2) {
                this.score2++;
                //p2 win
            }
        }

        if (filled[0][0] == 1 && filled [1][1] == 1 && filled [2][2] == 1) {
            this.score1++;
            //p1 win
        }
        if (filled[0][0] == 2 && filled [1][1] == 2 && filled [2][2] == 2) {
            this.score2++;
            //p2 win
        }

        if (filled[0][2] == 1 && filled [1][1] == 1 && filled [2][0] == 1) {
            this.score1++;
            //p1 win
        }
        if (filled[0][2] == 2 && filled [1][1] == 2 && filled [2][0] == 2) {
            this.score2++;
            //p2 win
        }

    }

}
