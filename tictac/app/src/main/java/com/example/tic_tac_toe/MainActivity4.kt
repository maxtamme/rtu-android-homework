package com.example.tic_tac_toe

import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity4 : AppCompatActivity() {

    lateinit var plScore1 : TextView;
    lateinit var plScore2 : TextView;
    lateinit var one : Button;
    lateinit var two : Button;
    lateinit var three : Button;
    lateinit var four : Button;
    lateinit var five : Button;
    lateinit var six : Button;
    lateinit var seven : Button;
    lateinit var eight : Button;
    lateinit var nine : Button;
    lateinit var resetButton: Button;

    //0 = empty; 1 = pl; 2 =pl2;
    lateinit var filled : Array<IntArray>

    private var pl1Turn : Boolean = true;
    private var score1 : Int = 0;
    private var score2 : Int = 0;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main4)

        filled = arrayOf(
            intArrayOf(0, 0, 0),
            intArrayOf(0, 0, 0),
            intArrayOf(0, 0, 0)
        )

        Toast.makeText(this, "Good luck", Toast.LENGTH_SHORT).show()

        resetButton = findViewById(R.id.buttonReset)
        resetButton.setOnClickListener { reset() }
        one  = findViewById(R.id.Button1);
        one.setOnClickListener { action(0, 0, one); }
        two = findViewById(R.id.Button2);
        two.setOnClickListener { action(0, 1, two); }
        three = findViewById(R.id.Button3);
        three.setOnClickListener { action(0, 2, three); }
        four = findViewById(R.id.Button4);
        four.setOnClickListener { action(1, 0, four); }
        five = findViewById(R.id.Button5);
        five.setOnClickListener { action(1, 1, five); }
        six = findViewById(R.id.Button6);
        six.setOnClickListener { action(1, 2, six); }
        seven = findViewById(R.id.Button7);
        seven.setOnClickListener { action(2, 0, seven); }
        eight = findViewById(R.id.Button8);
        eight.setOnClickListener { action(2, 1, eight); }
        nine = findViewById(R.id.Button9);
        nine.setOnClickListener { action(2, 2, nine); }

        plScore1 = findViewById(R.id.scorePl1);
        plScore2 = findViewById(R.id.scorePl2);

        var j : Int = 0;

        for ((i, arr) in filled.withIndex()) {
            j = 0;
            for (zxc in arr) {
                filled[i][j] = 0;
                j++;
            }
        }

    }

    private fun action(i : Int, j : Int, but : Button) {
        changeColor(i, j, but)
    }

    private fun checkWin(): Boolean {
        var field : Array<Array<String>> = arrayOf(
            arrayOf(one.text.toString(), two.text.toString(), three.text.toString()),
            arrayOf(four.text.toString(), five.text.toString(), six.text.toString()),
            arrayOf(seven.text.toString(), eight.text.toString(), nine.text.toString())
        )
        for (i in 0..2) {
            if (field[i][0].equals(field[i][1])
                && field[i][0].equals(field[i][2])
                && !field[i][0].equals("")
            ) {
                return true
            }
        }
        for (i in 0..2) {
            if (field[0][i].equals(field[1][i])
                && field[0][i].equals(field[2][i])
                && !field[0][i].equals("")
            ) {
                return true
            }
        }
        if (field[0][0].equals(field[1][1])
            && field[0][0].equals(field[2][2])
            && !field[0][0].equals("")
        ) {
            return true
        }
        if (field[0][2].equals(field[1][1])
            && field[0][2].equals(field[2][0])
            && !field[0][2].equals("")
        ) {
            return true
        }
        var draw : Boolean = true;
        for (i in 0..2) {
            for (j in 0..2) {
                if (field[i][j] == "") draw = false
            }
        }
        if (draw) Toast.makeText(this, "Draw", Toast.LENGTH_SHORT).show()
        return false;
    }

    private fun changeColor(i: Int, j: Int, but : Button) {
        if (but.text != "") return
        if (pl1Turn) {
            but.text = "X"
            pl1Turn = false;
        }
        else {
            but.text = "O"
            pl1Turn = true;
        }
        if (checkWin()) {
            if (!pl1Turn) {
                score1++;
                Toast.makeText(this, "Player 1 win", Toast.LENGTH_SHORT).show()
                plScore1.text = "Player 1: ".plus(score1)
                disable()
            }
            else {
                score2++;
                Toast.makeText(this, "Player 2 win", Toast.LENGTH_SHORT).show()
                plScore2.text = "Player 2: ".plus(score2)
                disable()
            }
        }
    }

    private fun disable() {
        one.isEnabled = false;
        two.isEnabled = false;
        three.isEnabled = false;
        four.isEnabled = false;
        five.isEnabled = false;
        six.isEnabled = false;
        seven.isEnabled = false;
        eight.isEnabled = false;
        nine.isEnabled = false;
    }

    private fun reset() {
        pl1Turn = true;
        one.text = "";
        two.text = "";
        three.text = "";
        four.text = "";
        five.text = "";
        six.text = "";
        seven.text = "";
        eight.text = "";
        nine.text = "";
        one.isEnabled = true;
        two.isEnabled = true;
        three.isEnabled = true;
        four.isEnabled = true;
        five.isEnabled = true;
        six.isEnabled = true;
        seven.isEnabled = true;
        eight.isEnabled = true;
        nine.isEnabled = true;
    }

}